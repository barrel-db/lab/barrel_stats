-module(barrel_stats_counter).

-export([init/1, init/2,
         inc/2, inc/1,
         value/1,
         reset/1]).

init(Name) ->
  mzmetrics:alloc_resource(0, Name, 8).

init(Name, InitValue) ->
  Ref = mzmetrics:alloc_resource(0, Name, 8),
  mzmetrics:update_resource_counter(Ref, 0, InitValue).

inc(Ref) ->
   mzmetrics:incr_resource_counter(Ref, 0, 1).

inc(Ref, Val) when is_integer(Val), Val >= 0 ->
  mzmetrics:incr_resource_counter(Ref, 0, Val);
inc(Ref, Val) when is_float(Val) ->
  inc(Ref, round(Val));
inc(_, _) ->
  erlang:error(badarg).

value(Ref) ->
  mzmetrics:get_resource_counter(Ref, 0).

reset(Ref) ->
  mzmetrics:reset_resource_counter(Ref, 0).
