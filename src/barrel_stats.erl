-module(barrel_stats).


-export([register_name/2,
         whereis_name/1,
         unregister_name/1,
         registry_table/1]).

-export([start_link/0]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
         code_change/3]).

-define(TAB, ?MODULE).


register_name(Name, Pid) -> gen_server:call(?MODULE, {register, Name, Pid}).

unregister_name(Name) -> gen_server:call(?MODULE, {unregister, Name}).

whereis_name(Name) ->
  case ets:lookup(?TAB, Name) of
    [{Name, {Pid, _}}] ->
      case is_process_alive(Pid) of
        true -> Pid;
        false -> undefined
      end;
    [] ->
      undefined
  end.

registry_table(Name) ->
  case ets:lookup(?TAB, Name) of
    [{Name, {Pid, TabName}}] ->
      case is_process_alive(Pid) of
        true -> TabName;
        false -> undefined
      end;
    [] ->
      undefined
  end.

start_link() ->
  IsNew = init_tab(),
  gen_server:start_link({local, ?MODULE}, ?MODULE, [IsNew], []).

init_tab() ->
  case ets:info(?TAB, name) of
    undefined ->
      _ = ets:new(?TAB, [ordered_set, named_table, public, {read_concurrency, true}]),
      true;
    _ ->
      false
  end.

init([IsNew]) ->
  InitState = #{ monitors => init_monitors(IsNew) },
  {ok, InitState}.

init_monitors(true) -> #{};
init_monitors(false) ->
  Monitors = lists:foldl(fun({Name, {Pid, _}}, Monitors1) ->
                             %% reinit the table as well
                             Tab = init_registry_table(Name),
                             _ = ets:insert(Name, {Pid, Tab}),
                             %% remonitor the process
                             MRef = erlang:monitor(process, Pid),
                             Monitors1#{ Pid => {Name, MRef} }
                         end, #{}, ets:tab2list(?TAB)),
  Monitors.


handle_call({register, Name, Pid}, _From, State) ->
  State2 = do_register(Name, Pid, State),
  {reply, yes, State2};

handle_call({unregister, Name}, _From, State) ->
  State2 = do_unregister(Name, State),
  {reply, ok, State2};

handle_call(Request, From, State) ->
    error_logger:warning_msg("~p received an unexpected message:\n"
                             "handle_call(~p, ~p, _)\n",
                             [?MODULE_STRING, Request, From]),
    {noreply, State}.

handle_cast(_Msg, State) -> {noreply, State}.

handle_info({'DOWN', _MRef, process, Pid, _Info}, State) ->
  State2 = registry_is_down(Pid, State),
  {noreply, State2};

handle_info(_Info, State) -> {noreply, State}.

terminate(_Reason, _State) -> ok.

code_change(_OldVsn, State, _Extra) -> {ok, State}.

do_register(Name, Pid, State = #{ monitors := Monitors }) ->
  case ets:lookup(?MODULE, Name) of
    [{Name, _}] -> State;
    error ->
      MRef = erlang:monitor(process, Pid),
      Monitors2 = Monitors#{ Pid => {Name, MRef} },
      Tab = init_registry_table(Name),
      _ = ets:insert(Name, {Pid, Tab}),
      State#{ monitors => Monitors2 }
  end.

%% TODO: maybe move the control of the table to another process?
init_registry_table(Name) ->
  TabName = registry_name(Name),
  _ = ets:new(TabName, [ordered_set, public, {read_concurrency, true}]).

registry_name(Name) ->
  list_to_atom(atom_to_list(Name) ++ "_barrel_stats_registry").

do_unregister(Name, State = #{ monitors := Monitors }) ->
  case ets:take(?TAB, Name) of
    [] -> State;
    [{Name, {Pid, _TabName}}] ->
      {{Name, MRef}, Monitors2} = maps:take(Pid, Monitors),
      _ = erlang:demonitor(MRef, [flush]),
      State#{ monitors => Monitors2 }
  end.

registry_is_down(Pid, State = #{ monitors := Monitors }) ->
  case ets:take(Pid, Monitors) of
    {{Name, _MRef}, Monitors2} ->
      _ = ets:delete(?TAB, Name),
      State#{ monitors => Monitors2 };
    error ->
      State
  end.

