-module(barrel_stats_gauge).

-export([init/1, init/2,
         inc/1, inc/2,
         dec/1, dec/2,
         set/2,
         set_to_current_time/1,
         value/1,
         reset/1,
         timeit/2, timeit/3, timeit/4]).


init(Name) ->
  mzmetrics:alloc_resource(0, Name, 8).

init(Name, InitValue) ->
  Ref = mzmetrics:alloc_resource(0, Name, 8),
  _ = mzmetrics:update_resource_counter(Ref, 0, InitValue),
  Ref.

inc(Ref) -> inc(Ref, 1).

inc(Ref, Val) when is_integer(Val), Val >= 0 ->
  mzmetrics:incr_resource_counter(Ref, 0, Val);
inc(Ref, Val) when is_float(Val) ->
  inc(Ref, round(Val));
inc(_, _) ->
  erlang:error(badarg).

dec(Ref) -> dec(Ref, 1).

dec(Ref, Val) when is_integer(Val), Val >= 0 ->
  mzmetrics:decr_resource_counter(Ref, 0, Val);
dec(Ref, Val) when is_float(Val) ->
  dec(Ref, round(Val));
dec(_, _) ->
  erlang:error(badarg).

set(Ref, Val) ->
  mzmetrics:update_resource_counter(Ref, 0, Val).

set_to_current_time(Ref) ->
  set(Ref, barrel_stats_lib:now()).

value(Ref) ->
  mzmetrics:get_resource_counter(Ref, 0).

reset(Ref) ->
  mzmetrics:reset_resource_counter(Ref, 0).

timeit(Ref, F) ->
  T1 = erlang:monotonic_time(),
  Val = F(),
  T2 = erlang:monotonic_time(),
  Time = erlang:convert_time_unit(T2 - T1, native, second),
  set(Ref, Time),
  Val.

timeit(Ref, F, A) ->
  T1 = erlang:monotonic_time(),
  Val = apply(F, A),
  T2 = erlang:monotonic_time(),
  Time = erlang:convert_time_unit(T2 - T1, native, second),
  set(Ref, Time),
  Val.

timeit(Ref, M, F, A) ->
  T1 = erlang:monotonic_time(),
  Val = apply(M, F, A),
  T2 = erlang:monotonic_time(),
  Time = erlang:convert_time_unit(T2 - T1, native, second),
  set(Ref, Time),
  Val.
