-module(barrel_stats_registry).

-export([reg/1, reg/2,
         unreg/1, unreg/2]).

-export([start_link/1]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
         code_change/3]).


-include("barrel_stats.hrl").
-include_lib("stdlib/include/ms_transform.hrl").

reg(Spec) ->
  reg(?DEFAULT_REGISTRY, Spec).

reg(Registry, Spec) ->
  gen_server:call({via, barrel_stats, Registry}, {reg, Spec}).

unreg(Name) -> unreg(?DEFAULT_REGISTRY, Name).

unreg(Registry, Name) ->
  gen_server:call({via, barrel_stats, Registry}, {unreg, Name}).


start_link(Name) ->
  gen_server:start_link({via, barrel_stats, Name}, ?MODULE, [Name], []).


%% =========================
%% gen_server
%% =========================

init([Name]) ->
  InitState = #{ name => Name,
                 collectors => #{},
                 specs => #{} },
  {ok, InitState}.

handle_call({reg, Spec}, _From, State) ->
  {Reply, State2} = do_register(Spec, State),
  {reply, Reply, State2};

handle_call(_Msg, _From, State) -> {reply, bad_call, State}.

handle_cast(_Msg, State) -> {noreply, State}.

handle_info(_Info, State) -> {noreply, State}.

terminate(_Reason, _State) -> ok.

code_change(_OldVsn, State, _Extra) -> {ok, State}.

%% =========================
%% internals
%% =========================

do_register(#{ type := collector, name := default }, State) ->
  %% default collector always exist
  {{error, already_exists}, State};
do_register(#{ type := collector, name := Name } , State) ->
  %% register all metrics specifications in the registry. We do it in 2 pass
  %% to check if a metric has already been registered for this name.
  case validate_collector(Name, State) of
    ok ->
      Specs = Name:init(),
      case validate_specs(Specs, State) of
        ok -> register_specs(Specs, Name, State);
        Error -> {Error, State}
      end;
    Error ->
      Error
  end;
do_register(Metric, State) ->
  %% directly register a metric in default collector
  case validate_spec(Metric, State) of
    ok -> register_metric(Metric, default, State);
    Error -> Error
  end.


validate_collector(Name, #{ collectors := Collectors }) ->
  case maps:find(Name, Collectors) of
    {ok, _} -> ok;
    error -> {error, already_exists}
  end.


validate_specs([Spec | Rest], State) ->
  case validate_spec(Spec, State) of
    ok -> validate_specs(Rest, State);
    Error -> Error
  end;
validate_specs([], _State) ->
  ok.

validate_spec(#{ type := Type, name := Name, help := _Help }, State) ->
  #{ specs := Specs } = State,
  case validate_type(Type) of
    ok ->
      case validate_name(Name) of
        ok ->
          case maps:find(Name, Specs) of
            {ok, _} -> {error, already_exists};
            error -> ok
          end;
        Error ->
          Error
      end;
    Error ->
      Error
  end;
validate_spec(_, _) ->
  {error, invalid_spec}.


validate_type(counter) -> ok;
validate_type(gauge) -> ok;
validate_type(histogram) -> ok;
validate_type(_) -> {error, invalid_metric}.

validate_name(Name) when is_atom(Name) ->
  NameStr = atom_to_list(Name),
  Parts = string:tokens(NameStr, "_"),
  HasNameSpace = length(Parts) >= 2,
  case HasNameSpace of
    true -> ok;
    false -> {error, invalid_name}
  end.

register_specs([Metric | Rest], Collector, State) ->
  NState = register_metric(Metric, Collector, State),
  register_specs(Rest, Collector, NState);
register_specs([], _Collector, State) ->
  {ok, State}.

register_metric(Metric, Collector, State = #{ specs := Specs }) ->
  #{ name := Name,  type := Type } = Metric,
  case metrics_mod(Type) of
    Mod when is_atom(Mod) ->
      Specs2 = Specs#{ Name => Metric#{ mod => Mod, collector => Collector } },
      {ok, State#{ specs := Specs2 }};
    Error ->
      Error
  end.

metrics_mod(counter) -> barrel_stats_counter;
metrics_mod(gauge) -> barrel_stats_gauge;
metrics_mod(histogram) -> barrel_stats_histogram;
metrics_mod(_) -> {error, undef}.
